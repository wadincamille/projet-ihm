const express = require('express');
//var asyncLib = require('async');
const yts = require("youtube-sr").default;
const ytext = require("youtube-ext");
//const yts = require( 'yt-search' );
var routeur = express();
routeur.listen(4455);
routeur.use(express.json());
routeur.use(express.urlencoded({ extended: true }));

 //const query = "https://youtu.be/TGJ9-1LWFtE";

// async function autreApi (req,res) {
//     try {
//         const info = await ytext.videoInfo(query);
//         console.log(info);
//         return res.json(info);
//     } catch (err) {
//         console.log(`No result were found for ${query} (${err})`);
//     }
// }

// const query = "believer imagine dragon";

// async function autreApi (req,res) {
//     try {
//         const {videos,channels,playlists } = await ytext.search(query);
//         //console.log(videos,channels,playlists);
//         console.log(videos)
//         let resultat = []
//         resultat.push(videos)
//         asyncLib.waterfall([
//             function(done){
//                 console.log("debut");
//                 let i =0
//                 videos.forEach(  function(v,_,arr) {
                    
//                     try{
//                         ytext.videoInfo(v.url).then( (resu) =>{
                            
//                             //resultat.push(resu);
//                             // console.log("forEach");
//                             // console.log("fin");
//                             i++;
//                             if(i===arr.length-1)
//                                 done();
//                         }
//                         );
                        
//                     }catch(err){
//                         console.log(`(video info version)No result were found for ${query} (${err})`);
//                     }
//                 });
                
//             },
//             function(done) {
//                 //console.log("rendu");
//                 console.log(resultat);
//                 return res.json(resultat);
//                 done();
//             }]);
//         } catch (err) {
//         console.log(`No result were found for ${query} (${err})`);
//     }
// }

const query = "https://www.youtube.com/playlist?list=PLFgquLnL59ak5FwmTB7DRJqX3M2B1D7xI";

async function autreApi (req,res) {
    try {
        const resu = await ytext.playlistInfo(query);
        //console.log(videos,channels,playlists);
        console.log(resu)
        let videos = []
        
        for(let i=0;i<5;i++){
            videos.push(resu.videos[i])
        }
        var thumb = [];
        var titre = [];
        //var view = [];
        var duration = [];
        var chan = [];
        //var posted = [];
        videos.forEach( function ( v,i,arr ) {
            if(i<5){
                const views = String( v.views ).padStart( 10, ' ' );
                console.log( `${ views } | ${ v.title } (${ v.durationFormatted }) | ${ v.channel.name }` );
                titre.push(v.title);
                thumb.push('https://img.youtube.com/vi/'+v.id+'/0.jpg');
                //view.push(v.views.pretty);
                duration.push(v.duration.text);
                chan.push(v.channel.name);
                //posted.push(v.published.pretty);
            }
        } )
        //console.log( r.title + ` (${ r.duration.timestamp })` )
    
        return res.json({"Access-Control-Allow-Origin":"*", 
        'titre':titre,
        'thumbnail':thumb,
        //'view':view,
        'duration':duration,
        'channel':chan,
        //'upload':posted
     });

    } catch (err) {
        console.log(`No result were found for ${query} (${err})`);
    }
}

async function main(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log(req.params.id);
    var recherche = req.params.id;
    const {videos,c,playlists} = await ytext.search(recherche);
    var thumb = [];
    var titre = [];
    var view = [];
    var duration = [];
    var chan = [];
    var posted = [];
    var url = []
    videos.forEach( function ( v,i,arr ) {
        if(i<5){
            const views = String( v.views ).padStart( 10, ' ' );
            //console.log( `${ views } | ${ v.title } (${ v.duration.text }) | ${ v.channel.name }` );
            titre.push(v.title);
            thumb.push('https://img.youtube.com/vi/'+v.id+'/0.jpg');
            view.push(v.views.prettyLong);
            duration.push(v.duration.text);
            chan.push(v.channel.name);
            posted.push(v.published.pretty);
            url.push(v.id);
        }
    } )
    //console.log( r.title + ` (${ r.duration.timestamp })` )

    return res.json({"Access-Control-Allow-Origin":"*", 
    'titre':titre,
    'thumbnail':thumb,
    'view':view,
    'duration':duration,
    'channel':chan,
    'upload':posted,
    'url':url
 });
}

async function empty(req,res){
    res.header("Access-Control-Allow-Origin", "*");
    const r = await yts.getPlaylist("PLFgquLnL59ak5FwmTB7DRJqX3M2B1D7xI");
    var thumb = [];
    var titre = [];
    var desc = [];
    var duration = [];
    var chan = [];
    var posted = [];
    var url = [];
    console.log(r);
    r.videos.forEach( function ( v ) {
	    const views = String( v.views ).padStart( 10, ' ' );
	    //console.log( `${ views } | ${ v.title } (${ v.duration }) | ${ v.channel.name } | ${ v.uploadedAt }| ${ v.description }` );
        titre.push(v.title);
        thumb.push('https://img.youtube.com/vi/'+v.id+'/0.jpg');
        desc.push(v.description);
        duration.push(v.durationFormatted);
        chan.push(v.channel.name);
        posted.push(v.uploadedAt);
        //console.log(v.id);
        url.push(v.id);
        
    } )

    return res.json({"Access-Control-Allow-Origin":"*", 
    'titre':titre,
    'thumbnail':thumb,
    //'description':desc,
    'duration':duration,
    'channel':chan,
    'url':url
    //'upload':posted
 });
}

/**
 * chose a renvoyé
 * description
 * published
 * nb Vue
 * Keyword
 * */ 
async function moreInfo(req,res) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log(req.params.id);
    var videoId = req.params.id;
    const video = await ytext.videoInfo("https://www.youtube.com/watch?v="+videoId);
    var thumb = video.thumbnails[video.thumbnails.length-1].url;
    var titre = video.title;
    var view = video.views;
    var duration = video.duration;
    var chan = video.channel;
    var posted = video.published;
    var description = video.shortDescription;
    var keyword = video.keywords;
    var like = video.ratings.likes;

    return res.json({"Access-Control-Allow-Origin":"*", 
    'titre':titre,
    'thumbnail':thumb,
    'view':view,
    'duration':duration,
    'channel':chan,
    'upload':posted,
    'desc':description,
    'keyword':keyword,
    'like':like
 });
}

routeur.route("/autre").get(autreApi);
routeur.route("/").get(empty);
routeur.route("/:id").get(main);
routeur.route("/moreInfo/:id").get(moreInfo);
