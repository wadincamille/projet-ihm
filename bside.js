function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  
  /* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
  function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
  } 

async function imgGet(){
    var req = document.getElementById("recherche");
    //console.log(req.value)
    //var img = document.createElement("img");
    const response = await fetch('http://localhost:4455/'+req.value);
    const names = await response.json();
    //console.log(names);
    i=0;
    var title;
    var img;
    var info;
    var chaine;
    var duree;
    var comment;
    while (i<5) {

        img =document.getElementById('img'+i);
        img.setAttribute('src',names.thumbnail[i]);

        info=document.getElementById('info');
        if(req.value == ""){
            info.textContent="Tendance";
        }else{
            info.textContent=req.value;
          
        }

        title = document.getElementById("div"+i);
        title.textContent=names.titre[i];

            document.getElementById("title"+i).textContent=names.titre[i];

        
        chaine =document.createElement('p');
        chaine.setAttribute('class', 'card__text');
        chaine.setAttribute('id',"p"+i);
        chaine.textContent="Chaines : "+names.channel[i];
        document.getElementById("div"+i).appendChild(chaine);

        duree =document.createElement('p');
        duree.setAttribute('class', 'card__text');
        duree.setAttribute('id',"p"+i+"2");
        duree.textContent="Durée  : "+names.duration[i];
        document.getElementById("div"+i).appendChild(duree);
        
        var span =document.createElement('span');
        span.setAttribute('class', 'hidden');
        span.setAttribute('id',"url"+i);
        span.textContent=names.url[i];
        document.getElementById("div"+i).appendChild(span);

        

        i++;
    }
    
}

async function infoGet(i){
    var req = document.getElementById("url"+i);
    console.log(req.textContent);
    //var img = document.createElement("img");
    const response = await fetch('http://localhost:4455/moreInfo/'+req.textContent);
    const names = await response.json();
    //console.log(names);
    var title;
    var img;
    var info;
    var vue;
    var duree;


    document.getElementById("descr"+i).textContent="Description : "+names.desc;
    document.getElementById("published"+i).textContent="Publication : "+names.upload.pretty;
    document.getElementById("nbVue"+i).textContent="Nombre de vue : "+names.view.pretty;
    document.getElementById("nbLike"+i).textContent="Nombre de j'aime : "+names.like.pretty;
    if(names.keyword===undefined){
        document.getElementById("tags"+i).textContent="Liste de mot clés : Pas de mots clés";
    }else{
        document.getElementById("tags"+i).textContent="Liste de mot clés : "+names.keyword;
    }
    
    document.getElementById("imgNav"+i).setAttribute("src",names.thumbnail);
    document.getElementById("link"+i).setAttribute("href","https://www.youtube.com/watch?v="+req.textContent);
    document.getElementById("link2"+i).setAttribute("href","https://www.youtube.com/watch?v="+req.textContent);
    console.log(names);
    
    
}

window.onload = function() {
    document.getElementById('LeBoutton').onclick = imgGet;
   }