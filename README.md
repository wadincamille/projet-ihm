# Projet IHM Web

## Pour lancer l'application

Installer node.js **https://nodejs.org/en/**

Installer avec `npm install` les modules

```
npm install youtube-ext
npm install youtube-sr
npm install express
```

Commande pour lancer l'api (se mettre dans le dossier)

```
node index.js
```

L'api qui gère le back est maintenant lancée

---

Lancer [`index.html`](./index.html) pour accéder à l'application

![](https://cdn.discordapp.com/attachments/887249916139798568/918497713551720568/unknown.png "L'application affiche ce genre d'image pendant l'attente de réponse de l'api")

![](https://cdn.discordapp.com/attachments/887249916139798568/918495447784517683/unknown.png "Affichage au début de l'application")

![](https://cdn.discordapp.com/attachments/887249916139798568/918496910246039632/unknown.png "Affichage d'une recherche avec le barre d'information")

---

